using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Attributes;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;

namespace AzFunctionEchoSample
{
    public class SampleFunctions
    {
        private readonly ILogger<SampleFunctions> _logger;

        public SampleFunctions(ILogger<SampleFunctions> log)
        {
            _logger = log;
        }

        [FunctionName("EchoOkFunction")]
        [OpenApiOperation(operationId: "EchoOk")]
        [OpenApiRequestBody("text/json", typeof(string))]
        [OpenApiResponseWithBody(statusCode: HttpStatusCode.OK, contentType: "text/plain", bodyType: typeof(string), Description = "The body of the input request, echoed")]
        public async Task<IActionResult> EchoOk(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "get", Route = null)] HttpRequest req)
        {
            _logger.LogInformation("C# HTTP trigger function 200 processed a request.");

            string responseMessage = await GetResponseBody(req);

            return new OkObjectResult(responseMessage);
        }

        [FunctionName("EchoBadRequestFunction")]
        [OpenApiOperation(operationId: "EchoBadRequest")]
        [OpenApiRequestBody("text/json", typeof(string))]
        [OpenApiResponseWithBody(statusCode: HttpStatusCode.OK, contentType: "text/plain", bodyType: typeof(string), Description = "The body of the input request, echoed")]
        public async Task<IActionResult> EchoBadRequest(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "get", Route = null)] HttpRequest req)
        {
            _logger.LogInformation("C# HTTP trigger function 400 processed a request.");

            string responseMessage = await GetResponseBody(req);

            return new BadRequestObjectResult(responseMessage);
        }

        private async Task<string> GetResponseBody(HttpRequest req)
        {
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();

            string responseMessage = string.IsNullOrEmpty(requestBody)
                ? "The request has no body"
                : requestBody;
            return responseMessage;
        }
    }
}

